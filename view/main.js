$(function() {

    let apiUrl = "https://ledev.ctro.space/social%20network/api/api.php";
    let token = "";

    // AutoInit for MaterializeCSS
    M.AutoInit();

    $.ajax({
        url: apiUrl,
        method: "post",
        data: ({
            action: "login",
            tag: "a",
            password: "a"
        }),

        success: (data) => {
            if (data && data.success) {
                token = data.token;
                setCookie('token', token)
            } else
                alert("Login failed!");
        },

        error: (error) => console.log(error)
    });


    function setCookie(name, value) {
        var expiresDays = 7; // The number of days in which the cookie will expire
        var date = new Date();
        date.setTime(date.getTime() + (expiresDays * 24 * 60 * 60 * 1000));
        // Encode the token to permit special characters
        document.cookie = name + "=" + encodeURIComponent(value) + ";expires=" + date.toUTCString() + ";path=/;secure";
    }

    function readCookie(name) {
        var key = name + "=";
        // Decode the token to retrieve special characters
        var list = decodeURIComponent(document.cookie).split(';');
        for (var i = 0; i < list.length; i++) {
            var field = list[i];
            // Remove the blank spaces
            while (field.charAt(0) == ' ')
                field = field.substring(1);
            // If the key is found starting from the beggining of the field
            if (field.indexOf(key) == 0)
                return field.substring(key.length, field.length);
        }

        // If nothing is found
        return false;
    }
});
