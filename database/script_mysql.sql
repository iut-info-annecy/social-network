#POSTGRESQL version of the script

DROP TABLE IF EXISTS sn_user;
DROP TABLE IF EXISTS sn_follow;
DROP TABLE IF EXISTS sn_follow_request;
DROP TABLE IF EXISTS sn_follow_authorization;
DROP TABLE IF EXISTS sn_post;
DROP TABLE IF EXISTS sn_content;
DROP TABLE IF EXISTS sn_cont_type;
DROP TABLE IF EXISTS sn_user_type;
DROP TABLE IF EXISTS sn_notification;
DROP TABLE IF EXISTS sn_notif_type;

CREATE TABLE sn_user (
	userid SERIAL AUTO_INCREMENT, #unique id for the database
	tag VARCHAR(30) NOT NULL, #unique identifier, can be seen by anyone, should be unique, plaintext
	name VARCHAR(100), #private name, only seen by the guy's friends, can be the same as someone else, encrypted with content_key
	pub_key TEXT NOT NULL, #public key, can be used by anyone to send this guy some messages that only him can decrypt, stored in plaintext
	priv_key TEXT NOT NULL, #private key, stored encrypted somehow using the user's password, used to decrypt :
							# the content key for this user
	content_key TEXT NOT NULL, #main symetric key, used to encrypt/decrypt all of the user's contents (profile, posts..), stored encrypted by pub_key
	pp_status INT NOT NULL, #used to know wether has already changed his profile picture or if he/she still has the default one
	profile_message TEXT, #message that appears on the profile page, encrypted with the author's content_key
	user_type INT NOT NULL, #role of the user, references user_type from sn_user_type
	email VARCHAR(50), #stored in plaintext
	CONSTRAINT pk_sn_user PRIMARY KEY (userid)
);

CREATE TABLE sn_user_type (
	user_type SERIAL AUTO_INCREMENT, #id for the user type
	wording VARCHAR(30), #for instance, "admin"
	CONSTRAINT pk_sn_user_type PRIMARY KEY (user_type)
);

CREATE TABLE sn_follow (
	follower INT NOT NULL, #if A follows B, this is A's userid
	followed INT NOT NULL, #if A follows B, this is B's userid
	content_key TEXT NOT NULL #if A follows B, this is B's content_key, encrypted with A's pub_key
);

CREATE TABLE sn_follow_request ( #when A wants to follow B, he creates one to ask b for his content_key
	follower INT NOT NULL, #if A tries to follow B, this is A's userid
	followed INT NOT NULL #if A tries to follow B, this is B's userid
);

CREATE TABLE sn_follow_authorization ( #when B wants to allow A to follow him, he creates one in order to give A his content_key
	follower INT NOT NULL, #if B allows A to follow him, this is A's userid
	followed INT NOT NULL, #if B allows A to follow him, this is B's userid
	content_key TEXT NOT NULL #if B allows A to follow him, this is B's content_key, encrypted with A's pub_key
);

#when both a sn_follow_authorization and sn_follow_request exist for the same follower/followed couple, they are deleted and a sn_follow is created

CREATE TABLE sn_post ( #a "post" is somehow the equivalent of a "tweet"
	postid SERIAL AUTO_INCREMENT, #id of this post for the database
	sharable BOOLEAN NOT NULL, #used to know wether other people can "retweet" it or not
	userid INT NOT NULL, #userid of the author of the post
	message TEXT NOT NULL, #main message of the post (the limit of 280 characters may be changed), stored encrypted with userid's content_key
	post_date TIMESTAMP, #date the post has been made at
	other_content INT, #used to know wether the post has additionnal content like a picture, video...
							#if NULL, this post doesn't have any additionnal content
							#references a contentid from the sn_content table
	share_of INT, #if NULL, this is the original post, if not, this a "retweet" of postid number share_of
	signature TEXT, #encrypted hash of the contents of the post
	CONSTRAINT pk_sn_post PRIMARY KEY (postid)
);

CREATE TABLE sn_content ( #a "content" is a video, an image... attached to a post
	contentid SERIAL AUTO_INCREMENT, #id of the content for the database
	#postid INT NOT NULL, #reference to the postid from sn_post this content is linked to
	cont_typeid INT NOT NULL, #reference to a cont_typeid from the sn_cont_type table
	file VARCHAR(50) NOT NULL, #name of the file, or whatever used to know where to file is, this is TO CHANGE	
	CONSTRAINT pk_sn_content PRIMARY KEY (contentid)
);

CREATE TABLE sn_cont_type ( #used to list every possible content type (picture, video...)
	cont_typeid SERIAL AUTO_INCREMENT,
	label VARCHAR(30) NOT NULL, # for instance, "PNG image" (wording means "libellé" in french)
	CONSTRAINT pk_sn_cont_type PRIMARY KEY (cont_typeid)
);

CREATE TABLE sn_notification (
	notificationid SERIAL AUTO_INCREMENT,
	notif_type INT,
	link INT,
	notif_date TIMESTAMP,
	seen BOOLEAN,
	userid INT,
	CONSTRAINT pk_sn_notification PRIMARY KEY(notificationid)
);

CREATE TABLE sn_notif_type (
	notif_typeid SERIAL AUTO_INCREMENT,
	wording TEXT,
	CONSTRAINT pk_sn_notif_type PRIMARY KEY(notif_typeid)
);


ALTER TABLE sn_user CHANGE userid userid INT(11) AUTO_INCREMENT;
ALTER TABLE sn_user_type CHANGE user_type user_type INT(11) AUTO_INCREMENT;
ALTER TABLE sn_post CHANGE postid postid INT(11) AUTO_INCREMENT;
ALTER TABLE sn_content CHANGE contentid contentid INT(11) AUTO_INCREMENT;
ALTER TABLE sn_cont_type CHANGE cont_typeid cont_typeid INT(11) AUTO_INCREMENT;

ALTER TABLE sn_follow ADD CONSTRAINT fk_sn_follow_sn_user_follower FOREIGN KEY (follower) REFERENCES sn_user(userid) ON DELETE CASCADE;
ALTER TABLE sn_follow ADD CONSTRAINT fk_sn_follow_sn_user_followed FOREIGN KEY (followed) REFERENCES sn_user(userid) ON DELETE CASCADE;
ALTER TABLE sn_follow_request ADD CONSTRAINT fk_sn_follow_request_sn_user_follower FOREIGN KEY (follower) REFERENCES sn_user(userid) ON DELETE CASCADE;
ALTER TABLE sn_follow_request ADD CONSTRAINT fk_sn_follow_request_sn_user_followed FOREIGN KEY (followed) REFERENCES sn_user(userid) ON DELETE CASCADE;
ALTER TABLE sn_follow_authorization ADD CONSTRAINT fk_sn_follow_authorization_sn_user_follower FOREIGN KEY (follower) REFERENCES sn_user(userid) ON DELETE CASCADE;
ALTER TABLE sn_follow_authorization ADD CONSTRAINT fk_sn_follow_authorization_sn_user_followed FOREIGN KEY (followed) REFERENCES sn_user(userid) ON DELETE CASCADE;
ALTER TABLE sn_post ADD CONSTRAINT fk_sn_post_sn_user FOREIGN KEY (userid) REFERENCES sn_user(userid) ON DELETE CASCADE;
ALTER TABLE sn_content ADD CONSTRAINT fk_sn_content_sn_cont_type FOREIGN KEY (cont_typeid) REFERENCES sn_cont_type(cont_typeid);
ALTER TABLE sn_user ADD CONSTRAINT fk_sn_user_sn_user_type FOREIGN KEY (user_type) REFERENCES sn_user_type (user_type);
ALTER TABLE sn_follow_request ADD CONSTRAINT ck_unique_follower_followed_request UNIQUE (follower, followed);
ALTER TABLE sn_follow_authorization ADD CONSTRAINT ck_unique_follower_followed_authorization UNIQUE (follower, followed);
ALTER TABLE sn_follow ADD CONSTRAINT ck_unique_follower_followed_follow UNIQUE (follower, followed);


INSERT INTO sn_user_type VALUES (default, 'Admin');
INSERT INTO sn_user_type VALUES (default, 'User');
INSERT INTO sn_user_type VALUES (default, 'Unvalidated user');

INSERT INTO sn_cont_type VALUES (default, 'Image');
INSERT INTO sn_cont_type VALUES (default, 'Video');
INSERT INTO sn_cont_type VALUES (default, 'Audio');
INSERT INTO sn_cont_type VALUES (default, 'Link');

INSERT INTO sn_notif_type VALUES (default, 'Follow request');
INSERT INTO sn_notif_type VALUES (default, 'Follow accept');
INSERT INTO sn_notif_type VALUES (default, 'New follower');
INSERT INTO sn_notif_type VALUES (default, 'Follow post mentionning you'); #not yet implemented
INSERT INTO sn_notif_type VALUES (default, 'News about the social network');
