<?php

// Database loader

function getPDO() {
    $config = parse_ini_file('./db.ini');
    return new PDO($config['servername'] . ';dbname=' . $config['dbname'] . ';charset=utf8', $config['username'], $config['password']);
}

$db = getPDO();
// TODO: Add global set below IF NEEDED ONLY