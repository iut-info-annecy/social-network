<?php 

class Post {

	private $_postid;
	private $_sharable;
	private $_userid;
	private $_plain_message;
	private $_encrypted_message;
	private $_post_date;
	private $_other_content;
	private $_share_of;
	private $_signature;
	private $_content_key;
	private $_encrypted_name;
	private $_tag;

	public static function AutoInit($postid) {
		$query = $GLOBALS['db']->prepare('SELECT * FROM sn_post WHERE postid=?');
		$query->execute(array($postid));

		if ($query->errorCode()==0 && $query->rowCount()==1) {
			$data = $query->fetch();
			$post = new Post($data['userid']);
			$post->init($data['postid'], $data['message'], $data['sharable'], $data['post_date'], $data['other_content'], $data['share_of'], $data['signature']);
			return $post;
		}
		else {
			return false;
		}
	}

	public function __construct($userid) {
		$this->_userid = $userid;
		$this->_plain_message = "";
		$this->_postid = 0;
		$this->_sharable = false;
		$this->_post_date = NULL;
		$this->_other_content = 0;
		$this->_share_of = 0;
		$this->_signature = NULL;
		$this->_encrypted_message = "";
		$this->_plain_message = "";
		$this->_content_key = "";
	}

	public function init($postid, $encrypted_message, $sharable, $post_date, $other_content, $share_of, $signature, $tag = NULL, $encrypted_name = NULL) {
		$this->_signature = $signature;
		if ($this->checkSignature($this->staticHashCode($this->_userid, $encrypted_message, $postid, (int)$sharable, $post_date, $other_content, $share_of)) === true) {
			$this->_postid = $postid;
			$this->_encrypted_message = $encrypted_message;
			$this->setSharable($sharable);
			$this->_post_date = $post_date;
			$this->setOtherContent($other_content);
			$this->setShareOf($share_of);
			$this->_signature = $signature;
			$this->_encrypted_name = $encrypted_name;
			$this->_tag = $tag;
			return true;
		}
		else {
			//throw new Exception('The signature of the post is wrong');
			echo 'Error. The signature of the post is wrong.<br>';
			return false;
		}
	}

	//returns string
	public function getHashCode() {
		$string = $this->_postid.$this->getSharable().$this->getUserID().$this->getEncryptedMessage().$this->getPostDate().$this->_other_content.$this->getShareOf();
		return getHash($string);
	}

	public function getUserID() {
		if ($this->_userid != "") {
			return $this->_userid;
		}
		else
			throw new Exception('The userid is NULL. Please set it and retry'); //should never happen
	}

	public function staticHashCode($userid,$encrypted_message,$postid,$sharable,$post_date,$other_content,$share_of) {
		$string = $postid.$sharable.$userid.$encrypted_message.$post_date.$other_content.$share_of;
		return getHash($string);
	}

	//returns nothing
	public function setSignature($priv_key) {
		$this->_signature = asymPrivateEncrypt($this->getHashCode(), $priv_key);
	}

	//returns bool
	public function checkSignature($hash_code = NULL) {
		$query = $GLOBALS['db']->prepare('SELECT pub_key FROM sn_user WHERE userid = ?');
		$query->execute(array($this->_userid));

		if ($query->errorCode()==0 && $query->rowCount()===1) {
			$data = $query->fetch();
			$pub_key = $data['pub_key'];
		}
		else {
			return false;
		}

		if ($hash_code===NULL) {
			$hash_code = $this->getHashCode();
		}

		return $hash_code === asymPublicDecrypt($this->_signature, $pub_key);
	}

	public function setPlainMessage($plain_message) {
		if (strlen($plain_message) <= $MAX_MESSAGE_SIZE) {
			$this->_plain_message = htmlspecialchars($plain_message);
			if ($this->_content_key != "") {
				$this->_encrypted_message = symEncrypt($this->_plain_message, $this->_content_key);
			}
			else {
				$this->_encrypted_message = "";
			}
			return true;
		}
		else {
			return false;
		}
	}

	public function getPlainMessage() {
		if ($this->_plain_message != "") {
			return $this->_plain_message;
		}
		else if ($this->_encrypted_message != "" && $this->_content_key != "") {
			$this->_plain_message = symDecrypt($this->_encrypted_message, $this->_content_key);
			return $this->_plain_message;
		}
		else {
			throw new Exception('The decrypted message is not set and either the content key or the encrypted message (or both of them) is not set. Unable to get plain message');
		}
	}

	public function setEncryptedMessage($encrypted_message) {
		$this->_encrypted_message = $encrypted_message;
		if ($this->_content_key != "") {
			$this->_plain_message = symDecrypt($this->encrypted_message, $this->_content_key);
		}
		else {
			$this->_plain_message = "";
		}
	}

	public function getEncryptedMessage() {
		if ($this->_plain_message != "" || $this->_encrypted_message != "") {
			if ($this->_encrypted_message != "") {
				return $this->_encrypted_message;
			}
			else if ($this->_content_key != ""){
				$this->_encrypted_message = symEncrypt($this->_plain_message, $this->_content_key);
				return $this->_encrypted_message;
			}
			else {
				throw new Exception('The encrypted message is not set and the content key is not defined. Cant give you the encrypted message');
			}
		}
		else {
			throw new Exception("You are requesting the ecrypted message from a post without any message set");
		}
	}

	public function getPostDate() {
		if ($this->_post_date != "") {
			return $this->_post_date;
		}
		else if ($this->_postid != 0) {
			$query = $GLOBALS['db']->prepare('SELECT post_date FROM sn_post WHERE postid=?');
			$query->execute(array($this->_postid));

			if ($query->errorCode() != 0 || $query->rowCount() !=1) {
				return false;
			} 
			else {
				$data = $query->fetch();
				$this->_post_date = $data['post_date'];
				return $this->_post_date;
			}
		}
		else {
			return false;
		}
	}

	//returns false if no additionnal content is found, returns array(string $file, int $cont_typeid) otherwise
	public function getOtherContent() {
		if ($this->_other_content != "") {
			$query = $GLOBALS['db']->prepare('SELECT * FROM sn_content WHERE contentid=?');
			$query->execute(array($this->_other_content));

			if ($query->errorCode()==0 && $query->rowCount()==1) {
				$data = $query->fetch();
				return array('contentid' => $this->_other_content, 'cont_typeid' => $data['cont_typeid']);
			}
			else {
				throw new Exception('Unable to get information about the content from the database. Has it been deleted ?');
			}
		}
		else {
			return false;
		}
	}

	public function setOtherContent($other_content) {
		if ($other_content != "") {
			$query = $GLOBALS['db']->prepare('SELECT contentid FROM sn_content WHERE contentid=?');
			$query->execute(array($other_content));

			if ($query->errorCode()==0 && $query->rowCount()==1) {
				$this->_other_content = (int)$other_content;
				return true;
			}
			else {
				return false;
			}
		}
		return false;
	}

	//returns false if not a share of anything, the id of the original post otherwise
	public function getShareOf() {
		return (int)$this->_share_of;
	}

	public function getSharable () {
		return (int)$this->_sharable;
	}

	public function setShareOf($share_of) {
	//ideally, there should be a check to wether the $share_of is actually the id of a real post, but asking the db may be kinda slow, so... for now, let's admitt no one makes mistakes
	//$this->_sharable == true;
		$this->_share_of = (int)$share_of;
		return true;
	}

	public function setSharable($sharable) {
		$this->_sharable = (int)$sharable;
		return true;
	}

	public function setContentKey($content_key) {
		$this->_content_key = $content_key;
	}

	public function getSignature($priv_key = "") {
		if ($this->checkSignature() ===true) {
			return $this->_signature;
		}
		else if ($priv_key != "") {
			$this->setSignature($priv_key);
			if ($this->checkSignature()===true) {
				return $this->_signature;
			}
			else {
				throw new Exception('For some unknown reason, we are not able to calculate the correct signature. Maybe check the private key is incorrect ?');
			}
		}
		else {
			throw new Exception('The signature is not set and priv_key is not set. Unable to get/set signature');
		}
	}

	public function getName() {
		if (isset($this->_encrypted_name) && $this->_encrypted_name != ""){
			if(isset($this->_content_key)) {
				return symDecrypt($this->_encrypted_name, $this->_content_key);
			}
			else {
				return false;
			}
		}
		else {
			$query = $GLOBALS['db']->prepare('SELECT tag, name FROM sn_user WHERE userid=?');
			$query->execute(array($this->getUserID()));

			if ($query->errorCode()==0){
				$data = $query->fetch();
				if (isset($data['tag'])){
					$this->_tag = $data['tag'];
				}
				if (isset($data['name'])) {
					$this->_encrypted_name = $data['name'];
				}
			}
			else {
				throw new Exception('Query to get tag and name failed. Aborting');//should never happen
			}
			if(isset($this->_content_key)) {
				return symDecrypt($this->_encrypted_name, $this->_content_key);
			}
			else {
				return false;
			}
		}
	}

	public function getTag() {
		if (isset($this->_tag) && $this->_tag != ""){
			return $this->_tag;
		}
		else {
			$query = $GLOBALS['db']->prepare('SELECT tag, name FROM sn_user WHERE userid=?');
			$query->execute(array($this->getUserID()));

			if ($query->errorCode()==0){
				$data = $query->fetch();
				if (isset($data['tag'])){
					$this->_tag = $data['tag'];
				}
				if (isset($data['name'])) {
					$this->_encrypted_name = $data['name'];
				}
			}
			else {
				throw new Exception('Query to get tag and name failed. Aborting');//should never happen
			}
			return $this->_tag;	
		}
	}

	public function submit($priv_key = NULL) {
		if ($priv_key != NULL) {
			$this->setSignature($priv_key);
		}
		
		if (isset($this->_postid) && $this->_postid != 0) {
			if ($this->checkSignature()===true) {
				$update = $GLOBALS['db']->prepare('UPDATE sn_post SET sharable=?,message=?,other_content=?,signature=? WHERE postid=?');
				$update->execute(array($this->getSharable(), $this->getEncryptedMessage(), $this->_other_content, $this->_signature, $this->_postid));
				$errorCode = $update->errorCode();
			}
			else 
				throw new Exception('Post signature is wrong. Impossible to insert it into the database. Maybe you should give the private key as a parameter to the sumbit function, or call setSignature() before');
		}
		else {
			$insert = $GLOBALS['db']->prepare("INSERT INTO sn_post VALUES(default,?,?,?,current_timestamp,?,?,?)");
			$insert->execute(array((int)$this->getSharable(), (int)$this->getUserID(), $this->getEncryptedMessage(), $this->_other_content, (int)$this->getShareOf(), 'placeholder'));

			if ($insert->errorCode()==0) {
				$get = $GLOBALS['db']->prepare('SELECT post_date, postid FROM sn_post WHERE postid=?');
				$get->execute(array($GLOBALS['db']->lastInsertId()));
				$data = $get->fetch();

				$this->_post_date = $data['post_date'];
				$this->_postid = $data['postid'];

				$update = $GLOBALS['db']->prepare('UPDATE sn_post SET signature=? WHERE postid=?');
				$update->execute(array($this->getSignature($priv_key), $this->_postid));
			}
			$errorCode = $update->errorCode();
		}
		return $errorCode==0;
	}

	public function getArray() {
		$array = array();
		$array['postid'] = $this->_postid;
		$array['sharable'] = $this->getSharable();
		$array['userid'] = $this->getUserID();
		$array['name'] = $this->getName();
		$array['tag'] = $this->getTag();
		$array['message'] = $this->getPlainMessage();
		$array['post_date'] = $this->getPostDate();
		$array['other_content'] = $this->getOtherContent();
		$array['share_of'] = $this->getShareOf();

		return $array;
	}

	public function getJson() {
		return json_encode($this->getArray());
	}
}

