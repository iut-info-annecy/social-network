<?php

class User {
    private $_email;
	private $_name;
	private $_tag;
    
	private $_privateKey;
    private $_publicKey;
	private $_contentKey;
    
	private $_userID;
	private $_profileMsg;
    private $_PPStatus;
    private $_userType;

    private $_feed;

	public function __construct() {
        $GLOBALS['db']->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
	}

    public static function getContentKeyOf($targetuser, $requester, $requester_priv_key) {
        $query = $GLOBALS['db']->prepare('SELECT content_key FROM sn_follow WHERE follower=? AND followed=?');
        $query->execute(array($requester, $targetuser));
        if ($query->errorCode()==0 && $query->rowCount()==1){
            $data = $query->fetch();
            return asymPrivateDecrypt($data['content_key'], $requester_priv_key);
        }
        else {
            return false;
        }
    }

	// The tag is the unique public username
	public function login($tag, $password) {
		$query = $GLOBALS['db']->prepare("SELECT * FROM sn_user WHERE tag = ?");
		$query->execute(array($tag));
        
        $row = $query->fetch();
        if (!$row)
            return false;
        
        $privateKey = symDecrypt($row['priv_key'], $password);
        
		if (substr($privateKey, 0, 27) === "-----BEGIN PRIVATE KEY-----") {
			$this->_privateKey = $privateKey;
            $this->_publicKey = $row['pub_key'];
			$this->_contentKey = base64_decode(asymPrivateDecrypt($row['content_key'], $this->_privateKey)) ;
            $this->_userID = $row['userid'];
			$this->_tag = $row['tag'];
            $this->_email = $row['email'];
			$this->_name = symDecrypt($row['name'], $this->_contentKey);
			$this->_profileMsg = symDecrypt($row['profile_message'], $this->_contentKey);
            $this->_PPStatus = $row['pp_status'];
            $this->_userType = $row['user_type'];

            $this->_feed = new Feed($this->_userID, $this->_privateKey, $this->_contentKey);
            
            return true;
		}

		return false;
	}

    public function init($tag, $priv_key = NULL, $requester = NULL) {
        $query = $GLOBALS['db']->prepare("SELECT * FROM sn_user WHERE tag = ?");
        $query->execute(array($tag));
        
        $row = $query->fetch();
        if (!$row)
            return false;

        $this->_publicKey = $row['pub_key'];
        $this->_userID = $row['userid'];
        $this->_tag = $tag;
        //$this->_email = $row['email'];
        $this->_PPStatus = $row['pp_status'];
        $this->_userType = $row['user_type'];

        if (isset($priv_key) && isset($requester)){
            $this->_contentKey = User::getContentKeyOf($this->_userID, $requester, $priv_key);
            $this->_name = symDecrypt($row['name'], $this->_contentKey);
            $this->_profileMsg = symDecrypt($row['profile_message'], $this->_contentKey);
        }

        return true;
    }
    
    public function changePassword($old, $new) {
        // Check the old password's validity
        $passwordCheckQuery = $GLOBALS['db']->prepare("SELECT priv_key FROM sn_user WHERE userid = ?");
        $passwordCheckQuery->execute(array($this->_userID));
        $privateKey = $passwordCheckQuery->fetch()['priv_key'];
        
        if (!$privateKey ||
            substr(symDecrypt($privateKey, $old), 0, 27) !== "-----BEGIN PRIVATE KEY-----") {
            return false;
        }
        
        $newPrivateKey = symEncrypt($this->_privateKey, $new);
        
        // The password is good, so update the private key with the new password
        $passwordChangeQuery = $GLOBALS['db']->prepare("UPDATE sn_user SET priv_key = ? WHERE userid = ?");
        return $passwordChangeQuery->execute(array($newPrivateKey, $this->_userID));
    }
    
    // Returns: the updated value, or false on fail
    private function updateField($fieldName, $newValue) {
        $query = $GLOBALS['db']->prepare("UPDATE sn_user SET " . htmlspecialchars($fieldName, ENT_QUOTES) . " = ? WHERE userid = ?");
        $securedValue = htmlspecialchars($newValue, ENT_QUOTES);
        if (!$query->execute(array($securedValue, $this->_userID)))
            return false;
        
        return $securedValue;
    }
    
    // Returns: String
    public function getEmail() {
        return $this->_email;
    }
    
    // Returns: bool
    public function setEmail($email) {
        $updatedValue = $this->updateField('email', $email);
        if ($updatedValue === false)
            return false;
        
        $this->_email = $updatedValue;
        return true;
    }
    
    // Returns: String
	public function getName() {
        return $this->_name;
    }
    
    // Returns: bool
    public function setName($name) {
        $updatedValue = $this->updateField('name', symEncrypt($name, $this->_contentKey));
        if ($updatedValue === false)
            return false;
        
        $this->_name = $updatedValue;
        return true;
    }
    
    // Returns: String
	public function getTag() {
        return $this->_tag;
    }
    
    // Returns: bool
    public function setTag($tag) {
        $updatedValue = $this->updateField('tag', $tag);
        if ($updatedValue === false)
            return false;
        
        $this->_tag = $updatedValue;
        return true;
    }
    
    // Returns: String
	public function getPrivateKey() {
        return $this->_privateKey;
    }
    
    // Returns: String
	public function getPublicKey() {
        return $this->_publicKey;
    }
    
    // Returns: String
	public function getContentKey() {
        return $this->_contentKey;
    }
    
    // Returns: String
	public function getUserID() {
        return $this->_userID;
    }
    
    // Returns: String
	public function getProfileMsg() {
        return $this->_profileMsg;
    }
    
    // Returns: bool
    public function setProfileMsg($profileMsg) {
        $updatedValue = $this->updateField('profile_message', symEncrypt($profileMsg, $this->_contentKey));
        if ($updatedValue === false)
            return false;
        
        $this->_profileMsg = $updatedValue;
        return true;
    }
    
    // Returns: int
	public function getPPStatus() {
        return $this->_PPStatus;
    }
    
    // Returns: bool
    public function setPPStatus($PPStatus) {
        $updatedValue = $this->updateField('pp_status', $PPStatus);
        if ($updatedValue === false)
            return false;
        
        $this->_PPStatus = $updatedValue;
        return true;
    }
    
    // Returns: int
	public function getUserType() {
        return $this->_userType;
    }

    public function makeFollowRequest($userid) {
        $makeRequest = $GLOBALS['db']->prepare('INSERT INTO sn_follow_request VALUES(?,?)');
        $makeRequest->execute(array($this->_userID,$userid));
        if ($makeRequest->errorCode()==0) {

            $checkReciprocity = $GLOBALS['db']->prepare('SELECT * FROM sn_follow_authorization WHERE follower=? AND followed=?');
            $checkReciprocity->execute(array($this->_userID, $userid));

            if ($checkReciprocity->errorCode()==0 && $checkReciprocity->rowCount()>=1) {
                $this->mergeFollow($this->_userID, $userid);
            }
            else {
                $notif = $GLOBALS['db']->prepare('INSERT INTO sn_notification VALUES(default, 1, ?, current_timestamp, false, ?)');
                $notif->execute(array($this->getUserID(), $userid));
            }
            return true;
        }
        else {
            return false;
        }
    }

    public function authorizeFollow($userid) {
        $getPubKey = $GLOBALS['db']->prepare('SELECT pub_key FROM sn_user WHERE userid=?');
        $getPubKey->execute(array($userid));

        if ($getPubKey->errorCode()==0) {
            $pubKey = ($getPubKey->fetch())['pub_key'];

            $authorize = $GLOBALS['db']->prepare('INSERT INTO sn_follow_authorization VALUES(?,?,?)');
            $authorize->execute(array($userid, $this->_userID,asymPublicEncrypt($this->_contentKey, $pubKey)));
            if ($authorize->errorCode()==0) {

                $checkReciprocity = $GLOBALS['db']->prepare('SELECT * FROM sn_follow_request WHERE follower=? AND followed=?');
                $checkReciprocity->execute(array($userid, $this->_userID));

                if ($checkReciprocity->errorCode()==0 && $checkReciprocity->rowCount()>=1) {
                    $this->mergeFollow($userid, $this->_userID);
                }
                else {
                    $notif = $GLOBALS['db']->prepare('INSERT INTO sn_notification VALUES(default, 2, ?, current_timestamp, false, ?)');
                    $notif->execute(array($this->getUserID(), $userid));
                }

                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }        
    }

    public function unFollow($userid) {
        $query = $GLOBALS['db']->prepare('DELETE FROM sn_follow WHERE follower=? AND followed=?');
        $query->execute(array($this->getUserID(), $userid));
        return $query->errorCode()==0;
    }

    private function mergeFollow($follower, $followed) { //I'm too lazy to make checks, so this should NEVER NEVER be used somewhere else than where it's currently used
        $notif = $GLOBALS['db']->prepare('INSERT INTO sn_notification VALUES(default, 3, ?, current_timestamp, false, ?)');
        $notif->execute(array($follower, $followed));

        $getInfo = $GLOBALS['db']->prepare('SELECT * FROM sn_follow_authorization WHERE follower=? AND followed=?');
        $getInfo->execute(array($follower, $followed));
        $data = $getInfo->fetch();

        $array = array('sn_follow_authorization', 'sn_follow_request');
        foreach ($array as $value) {
            $delete = $GLOBALS['db']->prepare('DELETE FROM '.$value.' WHERE follower=? AND followed=?');
            $delete->execute(array($follower, $followed));
        }
        $insert = $GLOBALS['db']->prepare('INSERT INTO sn_follow VALUES(?,?,?)');
        $insert->execute(array($data['follower'], $data['followed'], $data['content_key']));
    }
    //if somebody is courageous enought, he may implement checks to be sure every query succedeed

    public function getFollowers() {
        $followers = array();
        $getFollowers = $GLOBALS['db']->prepare('SELECT follower, sn_user.tag FROM sn_follow JOIN sn_user ON follower=userid WHERE followed=?');
        $getFollowers->execute(array($this->_userID));
        if ($getFollowers->errorCode()==0) {
            while ($data = $getFollowers->fetch()) {
                $followers[$data['follower']] = $data['tag'];
            }
            return $followers;
        }
        else {
            throw new Exception('Error while trying to get follower list. Please check the database is correct and you are logged in');
        }
    }

    public function getFollow() {
        $follow = array();
        $getFollow = $GLOBALS['db']->prepare('SELECT followed, name, tag,sn_follow.content_key FROM sn_follow JOIN sn_user ON followed=userid WHERE follower=?');
        $getFollow->execute(array($this->_userID));
        if ($getFollow->errorCode()==0) {
            while ($data = $getFollow->fetch()) {
                $follow[$data['followed']] = array('tag' => $data['tag'],'name' => symDecrypt($data['name'], asymPrivateDecrypt($data['content_key'], $this->_privateKey)));
            }
            return $follow;
        }
        else {
            throw new Exception('Error while trying to get the list of followed people. Please ensure the database is correct and you are logged in');
        }
    }

    public function getFollowAuthorizations() {
        $follow = array();
        $getFollow = $GLOBALS['db']->prepare('SELECT follower, tag FROM sn_follow_authorization JOIN sn_user ON follower=userid WHERE followed=?');
        $getFollow->execute(array($this->_userID));
        if ($getFollow->errorCode()==0) {
            while ($data = $getFollow->fetch()) {
                $follow[$data['follower']] = $data['tag'];
            }
            return $follow;
        }
        else {
            throw new Exception('Error while trying to get the list of people. Please ensure the database is correct and you are logged in');
        }
    }

    public function getFollowRequests() {
        $follow = array();
        $getFollow = $GLOBALS['db']->prepare('SELECT followed, tag FROM sn_follow_request JOIN sn_user ON followed=userid WHERE follower=?');
        $getFollow->execute(array($this->_userID));
        if ($getFollow->errorCode()==0) {
            while ($data = $getFollow->fetch()) {
                $follow[$data['followed']] = $data['tag'];
            }
            return $follow;
        }
        else {
            throw new Exception('Error while trying to get the list of people. Please ensure the database is correct and you are logged in');
        }
    }

    public function getJsonFeed($max = 9999999, $nb = 30) {
        return $this->_feed->getJsonFeed($max, $nb);
    }

    public function getFeed($max = 9999999, $nb = 30) {
        return $this->_feed->getFeed($max, $nb);
    }

    public function getArrayFeed($max = 9999999, $nb = 30) {
        return $this->_feed->getArrayFeed($max, $nb);
    }

    public function getArray() {
        $array = array();

        $array['userid'] = $this->getUserID();
        $array['tag'] = $this->getTag();
        //$array['pub_key'] = $this->getPublicKey();
        $array['pp_status'] = $this->getPPStatus();
        $array['user_type'] = $this->getUserType();

        if (isset($this->_contentKey)) {
            $array['name'] = $this->getName();
            $array['profile_message'] = $this->getProfileMsg();
        }

        return $array;
    }

    public function getNotifications($max = 9999999, $nb = 20) {
        $array = array();

        $query = $GLOBALS['db']->prepare('SELECT * FROM sn_notification WHERE userid=? AND notificationid<? LIMIT ?');
        $query->execute(array($this->getUserID(),$max, $nb));

        if ($query->errorCode()==0) {
            while($data = $query->fetch()) {
                $array2 = array();
                foreach ($data as $key => $value) {
                    if ($key!==(int)$key) {
                        $array2[$key] = $value;
                    }
                }
                $array[] = $array2;
            }
            return $array;
        }
        else {
            die(json_encode(array('success' => false)));
        }
    }
}
