<?php 

class Feed {

	private $_userid;
	private $_priv_key;
	private $_content_key;
	private $_posts = array();

	public function __construct($userid, $priv_key, $content_key) {
		$this->_userid = $userid;
		$this->_priv_key = $priv_key;
		$this->_content_key = $content_key;
	}

	private function generate($max, $nb) {
		$feed = array();
		$posts = $GLOBALS['db']->prepare('SELECT postid,sharable,sn_post.userid,message,post_date,other_content,share_of,signature,sn_user.tag, sn_user.name 
			FROM sn_post 
			JOIN sn_user ON sn_post.userid=sn_user.userid 
			WHERE sn_post.userid IN (SELECT followed FROM sn_follow WHERE follower=?) OR sn_post.userid=?
			AND postid<? ORDER BY postid desc LIMIT ?');
		$posts->execute(array($this->_userid,$this->_userid, $max, $nb));

		while ($data = $posts->fetch()) {
			$post = new Post($data['userid']);
			$post->init($data['postid'], $data['message'], $data['sharable'], $data['post_date'],$data['other_content'], $data['share_of'], $data['signature'], $data['tag'], $data['name']);

			if ($data['userid'] == $this->_userid) {
				$content_key = $this->_content_key;
			}
			else {
				$getKey = $GLOBALS['db']->prepare('SELECT content_key FROM sn_follow WHERE follower=? AND followed=?');
				$getKey->execute(array($this->_userid, $data['userid']));
				if ($getKey->errorCode()==0) {
					$content_key = ($getKey->fetch())['content_key'];
					$content_key = asymPrivateDecrypt($content_key, $this->_priv_key);
				}
				else {
					throw new Exception('Unable to get the key of one of the feed posts.');
				}
			}

			$post->setContentKey($content_key);
			$feed[] = $post;
		}
		$this->_posts = $feed;
	}

	public function getFeed($max = 9999999, $nb = 30) {
		$this->generate($max, $nb);
		return $this->_posts;
	}

	public function getJsonFeed($max = 9999999, $nb = 30) {
		$this->generate($max, $nb);
		$feed = array();
		foreach ($this->_posts as $post) {
			$feed[] = $post->getArray();
		}
		return json_encode($feed);
	}

	public function getArrayFeed($max = 9999999, $nb = 30) {
		$this->generate($max, $nb);
		$feed = array();
		foreach ($this->_posts as $post) {
			$feed[] = $post->getArray();
		}
		return $feed;
	}
}


