<?php 

$MAX_MESSAGE_SIZE = 280;
$DEFAULT_NUMBER_OF_POSTS = 20;

require('../DEV_bdd.php');
require('../functions/crypto.php');
require('../classes/post.php');
require('../classes/feed.php');
require('../classes/user.php');

session_start();

header('Content-Type: application/json; charset=utf-8');
if (isset($_SERVER['HTTP_ORIGIN']) && $_SERVER['HTTP_ORIGIN'] and preg_match("#^http(s)?://(\w*\.)?(((dev\.)?blueskyfr.space)|((\w*\.)?ctro.space)|77.204.155.29)/?$#", $_SERVER['HTTP_ORIGIN']))
    header('Access-Control-Allow-Origin: *');

function login($tag, $password) {
	$_SESSION['user'] = new User();
	$_SESSION['loggedin'] = $_SESSION['user']->login($_GET['tag'], $_GET['password']);
	$_SESSION['token'] = getHash(random_bytes(32));
	return array('success' => $_SESSION['loggedin'], 'token' => $_SESSION['token']);
}

function register($tag, $password, $email) {
	$asymKeyPair = generateAsymKey();

	$pub_key = $asymKeyPair['pub_key'];
	$priv_key = $asymKeyPair['priv_key'];

	$content_key_plain = base64_encode(generateSymKey());
	$content_key = asymPublicEncrypt($content_key_plain, $pub_key);
	$priv_key = symEncrypt($priv_key, $password);

	$query = $GLOBALS['db']->prepare("INSERT INTO sn_user VALUES(default, ?, ?, ?, ?, ?, 0, null, 2, ?)");
	$query->execute(array(htmlspecialchars($tag),symEncrypt(htmlspecialchars($tag), base64_decode($content_key_plain)), $pub_key, $priv_key, $content_key, htmlspecialchars($email)));

	return $query->errorCode() == 0;
}


function checkToken($token) {
	if (isset($_SESSION['token'])) {
		return $_SESSION['token'] == $token;
	}
	else {
		return false;
	}
}

function seeNotification($notificationid, $userid) {
	$query = $GLOBALS['db']->prepare('UPDATE sn_notification SET seen=true WHERE notificationid=? AND userid=?');
	$query->execute(array($notificationid, $userid));
	return $query->errorCode()==0;
}

if (!empty($_GET) && isset($_GET['action'])) {

	if ($_GET['action'] == 'login') {
		if (isset($_GET['tag']) && isset($_GET['password'])) {
			echo json_encode(array(login($_GET['tag'], $_GET['password'])));
		}
		else {
			echo json_encode(array('success' => false));
		}
	}
	else if ($_GET['action'] == 'register') {
		if (isset($_GET['tag']) && isset($_GET['password']) && isset($_GET['password2']) && isset($_GET['email'])) {
			if ($_GET['password'] == $_GET['password2']) {
				echo json_encode(array('success' => register($_GET['tag'], $_GET['password'], $_GET['email']))) ;
			}
			else {
				echo json_encode(array('success' => false, 'error' => 'The 2 passwords do not match'));
			}
		}
	}
	else if (isset($_SESSION['token']) && isset($_GET['token']) && checkToken($_GET['token'])===true && $_SESSION['loggedin'] === true) {
		if ($_GET['action'] == 'logout') {
			session_destroy();
			echo json_encode(array('success' => true));
		}
		else if ($_GET['action'] == 'getfeed') {
			if (isset($_GET['number'])) {
				$number = (int)$_GET['number'];
			}
			else {
				$number = $DEFAULT_NUMBER_OF_POSTS;
			}

			if (isset($_GET['lastpost'])) {
				$lastpost = (int)$_GET['lastpost'];
			}
			else {
				$lastpost = 99999999;
			}

			echo json_encode(array('success' => true, 'feed' => $_SESSION['user']->getArrayFeed($lastpost, $number))); 
		}
		else if($_GET['action'] == 'follow' && isset($_GET['user'])) {
			echo json_encode(array('success' => $_SESSION['user']->makeFollowRequest($_GET['user'])));
		}
		else if($_GET['action'] == 'unfollow' && isset($_GET['user'])) {
			echo json_encode(array('success' => $_SESSION['user']->unFollow($_GET['user'])));
		}
		else if($_GET['action'] == 'allowfollow' && isset($_GET['user'])) {
			echo json_encode(array('success' => $_SESSION['user']->authorizeFollow($_GET['user'])));
		}
		else if ($_GET['action'] == 'getfollow') {
			echo json_encode(array('success' => true, 'follow' => $_SESSION['user']->getFollow()));
		}
		else if ($_GET['action'] == 'getfollowers') {
			echo json_encode(array('success' => true, 'follow' => $_SESSION['user']->getFollowers()));
		}
		else if ($_GET['action'] == 'changepassword' && isset($_GET['password']) && isset($_GET['newpassword'])) {
			echo json_encode(array('success' => $_SESSION['user']->changePassword($_GET['password'], $_GET['newpassword'])));
		}
		else if ($_GET['action'] == 'changemessage' && isset($_GET['message'])) {
			echo json_encode(array('success' => $_SESSION['user']->setProfileMsg($_GET['message'])));
		}
		else if ($_GET['action'] == 'changename' && isset($_GET['name'])) {
			echo json_encode(array('success' => $_SESSION['user']->setName($_GET['name'])));
		}
		else if ($_GET['action'] == 'post' && isset($_GET['message']) && $_GET['message'] != "" && strlen($_GET['message'] <= $MAX_MESSAGE_SIZE)) {

			$post = new Post($_SESSION['user']->getUserID());

			$success = true;
			if (isset($_GET['sharable'])) {
				$success = $success && $post->setSharable($_GET['sharable']);
			}

			if (isset($_GET['other_content'])) {
				$success = $success && $post->setOtherContent($_GET['other_content']);
			}

			if (isset($_GET['share_of'])) {
				$success = $success && $post->setShareOf($_GET['share_of']);
			}
			$post->setEncryptedMessage(symEncrypt(htmlspecialchars($_GET['message']), $_SESSION['user']->getContentKey()));

			echo json_encode(array('success' => $success && $post->submit($_SESSION['user']->getPrivateKey())));
		}
		else if ($_GET['action'] == 'getuser' && isset($_GET['tag'])) {
			$user = new User();
			echo json_encode(array('success' => $user->init($_GET['tag'], $_SESSION['user']->getPrivateKey(), $_SESSION['user']->getUserID()), 'user' => $user->getArray()));
		}
		else if ($_GET['action'] == 'getpost' && isset($_GET['postid'])) {
			$post = Post::AutoInit($_GET['postid']);
			$post->setContentKey(User::getContentKeyOf($post->getUserID(), $_SESSION['user']->getUserID(), $_SESSION['user']->getPrivateKey()));
			echo json_encode(array('success' => $post !== false, 'post' => $post->getArray())) ;
		}
		else if ($_GET['action'] == 'getnotifications') {
			if (isset($_GET['max'])) {
				$max = (int)$_GET['max'];
			}
			else {
				$max = 99999999;
			}
			if (isset($_GET['nb'])) {
				$nb = (int)$_GET['nb'];
			}
			else {
				$nb = 20;
			}

			echo json_encode(array('success' => true, 'notifications' => $_SESSION['user']->getNotifications($max, $nb)));
		}
		else if ($_GET['action']=='seenotifications') {
			$result = true;
			if (isset($_GET['notifications'])) {
				$notifs = json_decode($_GET['notifications']);
				foreach ($notifs as $value) {
					$result = $result && seeNotification($value, $_SESSION['user']->getUserID());
				}
			}
			else {
				$result = false;
			}
			echo json_encode(array('success' => $result));
		}

		else {
			echo json_encode(array('success' => false, 'error' => 'Missing token or not logged in'));
		}

	}
	else {
		echo json_encode(array('success' => false, 'error' => 'Missing or wrong parameters'));
	}
}

