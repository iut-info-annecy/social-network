<?php

// Symmetrical

function generateSymKey() {
	return random_bytes(16);
}

function symEncrypt($message, $key) {
    $iv = random_bytes(16);
	return base64_encode(openssl_encrypt($message, "AES256", $key,0,$iv)).'-'.base64_encode($iv);
}

function symDecrypt($message, $key) {
    $messageAndIV = explode('-', $message);
    if (isset($messageAndIV[1])) {
        return openssl_decrypt(base64_decode($messageAndIV[0]), "AES256", $key,0, base64_decode($messageAndIV[1]));
    }
	else {
        return openssl_decrypt(base64_decode($messageAndIV[0]), "AES256", $key);
    }
}

// Asymmetrical

// Returns: ["priv_key" => String, "pub_key" => String]
function generateAsymKey() {
    $keyBundle = openssl_pkey_new(array('private_key_bits' => 2048));
    
    openssl_pkey_export($keyBundle, $privateKey);
    $publicKey = openssl_pkey_get_details($keyBundle)['key'];
    
    return ["priv_key" => $privateKey, "pub_key" => $publicKey];
}

function asymPublicEncrypt($data, $publicKey) {
    openssl_public_encrypt($data, $result, $publicKey);
    return base64_encode($result);
}

function asymPublicDecrypt($data, $publicKey) {
    openssl_public_decrypt(base64_decode($data), $result, $publicKey);
    return $result;
}

function asymPrivateEncrypt($data, $privateKey) {
    openssl_private_encrypt($data, $result, $privateKey);
    return base64_encode($result);
}

function asymPrivateDecrypt($data, $privateKey) {
    openssl_private_decrypt(base64_decode($data), $result, $privateKey);
    return $result;
}

function getHash($string){
    return hash('SHA256', $string);
}