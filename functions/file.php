<?php 

function setOtherContent($userid, $content_key) {

	$extension_upload = strtolower(substr(strrchr($_FILES['file']['name'],'.'),1));

	$extensionsImages = array('png', 'jpg', 'jpeg', 'gif', 'bmp');
	$extensionsVideos = array('mp4', 'avi');
	$extensionsAudio = array('mp3', 'wav');

	if (in_array($extension_upload, $extensionsImages)) {
		$type = 1;
	}
	else if (in_array($extension_upload, $extensionsVideos)){
		$type = 2;
	}
	else if (in_array($extension_upload, $extensionsAudio)) {
		$type = 3;
	}
	else {
		throw new Exception('Unallowed file extension !');
	}

	$oldname = $_FILES['file']['name'];
	move_uploaded_file($_FILES['file']['tmp_name'],/*"../temp/".*/$_FILES['file']['name']);
	$file = file_get_contents(/*'../temp/'.*/$oldname);
	$name = base64_encode(random_bytes(16)).'.'.$extension_upload;

	$encryptedFile = symEncrypt($file, $content_key);
	file_put_contents("files/".$name , $encryptedFile);
	unlink(/*"../temp/".*/$oldname);

	$query = $GLOBALS['db']->prepare('INSERT INTO sn_content VALUES(default,?,?)');
	$query->execute(array($type, $name));

	if ($query->errorCode()==0) {
		return $GLOBALS['db']->lastInsertId();
	}
	else {
		return false;
	}
}

function getOtherContent($contentid, $content_key) {
	$get = $GLOBALS['db']->prepare('SELECT file, cont_typeid FROM sn_content WHERE contentid=?');
	$get->execute(array($contentid));

	/*if ($get->errorCode()==0 && $get->rowCount()==1) {*/
		$data = $get->fetch();
		$file = file_get_contents("files/".$data['file']);
		$decrypted = symDecrypt($file, $content_key);
		return array("type" => $data['cont_typeid'], "file" => $decrypted);
	//}
}

