<?php 
//the database needs to have been included previously in $db global variable
//crypto.php needs to be incuded too

$max_name_size = 30;
$min_name_size = 4;
$max_profile_message_size = 500;
$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );


function login($tag, $password) { //if the login/password are correct, loads all user data in session variables and returns true, else returns false
								//session_start() also needs to have been executed previously

	$query = $GLOBALS['db']->prepare('SELECT * FROM sn_user WHERE tag=?');
	$query->execute(array($tag));

	if ($query->rowCount() == 1) {
		$userdata = $query->fetch();
		$priv_key = symDecrypt($userdata['priv_key'], $password);

		if (substr($priv_key,0,27) === "-----BEGIN PRIVATE KEY-----")
		{
			$content_key = base64_decode(asymPrivateDecrypt($userdata['content_key'], $priv_key));
			$profile_message = symDecrypt($userdata['profile_message'], $content_key);
			$name = symDecrypt($userdata['name'], $content_key);

			$_SESSION['userid'] = $userdata['userid'];
			$_SESSION['content_key'] = $content_key;
			$_SESSION['tag'] = $tag;
			$_SESSION['name'] = $name;
			$_SESSION['profile_message'] = $profile_message;
			$_SESSION['email'] = $userdata['email'];
			$_SESSION['priv_key'] = $priv_key;
			$_SESSION['pub_key'] = $userdata['pub_key'];
			$_SESSION['pp_status'] = $userdata['pp_status'];
			$_SESSION['user_type'] = $userdata['user_type'];
			$_SESSION['loggedin'] = true;

			return true;
		}
		else
		{
			return false;
		}
	}
	else {
		return false;
	}
}

function checkPassword($userid, $password, $encrypted_priv_key = NULL)
{
	if ($encrypted_priv_key === NULL)
	{
		$query = $GLOBALS['db']->prepare('SELECT * FROM sn_user WHERE userid=?');
		$query->execute(array($userid));

		if ($query->rowCount() == 1)
		{
			$userdata = $query->fetch();
			$encrypted_priv_key = $userdata['priv_key'];
		}
		else
		{
			return false;
		}
	}

	$priv_key = symDecrypt($encrypted_priv_key, $password);
	
	return substr($priv_key,0,27) === "-----BEGIN PRIVATE KEY-----";
}


function updateProfile($userid, $plain_content_key, $newName = NULL, $newProfile_message = NULL, $newPP_status = NULL)
{
	if ($newName != NULL)
	{
		if (strlen($newName) >= $GLOBALS['min_name_size'] && strlen($newName) <= $GLOBALS['max_name_size'])
		{
			$newName = symEncrypt(htmlspecialchars($newName), $plain_content_key);
			if (setEncryptedName($userid, $newName) === false)
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	if ($newProfile_message != NULL )
	{
		if (strlen($newProfile_message) <= $GLOBALS['max_profile_message_size'])
		{
			$newProfile_message = symEncrypt(htmlspecialchars($newProfile_message), $plain_content_key);
			if (setEncryptedProfileMessage($userid, $newProfile_message) === false)
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	if ($newPP_status !== NULL)
	{
		if (setPPStatus($userid, $newPP_status) === false)
		{
			return false;
		}
	}

	return true;
}

function setEncryptedName($userid, $encrypted_name)
{
	$updateName = $GLOBALS['db']->prepare('UPDATE sn_user SET name = ? WHERE userid = ?');
	$updateName->execute(array($encrypted_name, $userid));

	if ($updateName->errorCode() == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function setEncryptedProfileMessage($userid, $encrypted_profile_message)
{
	$updateProfileMessage = $GLOBALS['db']->prepare('UPDATE sn_user SET profile_message = ? WHERE userid = ?');
	$updateProfileMessage->execute(array($encrypted_profile_message, $userid));

	if ($updateProfileMessage->errorCode() == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function setPPStatus($userid, $pp_status)
{
	$updatePPStatus = $GLOBALS['db']->prepare('UPDATE sn_user SET pp_status = ? WHERE userid = ?');
	$updatePPStatus->execute(array($pp_status, $userid));

	if ($updatePPStatus->errorCode() == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function changePassword($userid ,$plain_priv_key ,$newPassword)
{
	$priv_key = symEncrypt($plain_priv_key, $newPassword);
	$updatePrivKey = $GLOBALS['db']->prepare('UPDATE sn_user SET priv_key=? WHERE userid=?');
	$updatePrivKey->execute(array($priv_key, $userid));

	if ($updatePrivKey->errorCode()==0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

?>
