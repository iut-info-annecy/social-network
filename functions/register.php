<?php 

function register($tag, $password, $email) {
	$asymKeyPair = generateAsymKey();

	$pub_key = $asymKeyPair['pub_key'];
	$priv_key = $asymKeyPair['priv_key'];

	$content_key_plain = base64_encode(generateSymKey());
	$content_key = asymPublicEncrypt($content_key_plain, $pub_key);
	$priv_key = symEncrypt($priv_key, $password);

	$query = $GLOBALS['db']->prepare("INSERT INTO sn_user VALUES(default, ?, ?, ?, ?, ?, 0, null, 1, ?)");
	$query->execute(array(htmlspecialchars($tag),symEncrypt(htmlspecialchars($tag), base64_decode($content_key_plain)), $pub_key, $priv_key, $content_key, htmlspecialchars($email)));
}
